package com.zuitt.b193;

import java.util.ArrayList;

public class Contact {

    //3.
    private String name;
    //4.
    private ArrayList<String> numbers = new ArrayList<>();
    private ArrayList<String> addresses = new ArrayList<>();

    //5.
    public Contact(){};
    public Contact(String name, String numbers, String addresses){
        this.name = name;
        this.numbers.add(numbers);
        this.addresses.add(addresses);
    }

    //6.
    public String getName() {
        return this.name;
    }

    public ArrayList<String> getNumbers() {
        return this.numbers;
    }

    public ArrayList<String> getAddresses() {
        return this.addresses;
    }

    //7.
    public void setName(String name) {
        this.name = name;
    }

    public void setNumbers(String numbers) {
        this.numbers.add(numbers);
    }

    public void setAddresses(String addresses) {

        this.addresses.add(addresses);
    }

    public void contactDetails(){

        System.out.println(getName());
        System.out.println("--------------------------------------------------");
        System.out.println(getName() + " has the following registered numbers:");
        for (String num: getNumbers()){
            System.out.println(num);
        }
        System.out.println("--------------------------------------------------");
        System.out.println(getName() + " has the following registered addresses:");
        for (String add: getAddresses()){
            System.out.println(add);
        }
        System.out.println("==================================================");
    }

}
