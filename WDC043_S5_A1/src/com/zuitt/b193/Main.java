package com.zuitt.b193;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args){

        //11.
        Phonebook phonebook = new Phonebook();

        //12.
        Contact con1 = new Contact();
        Contact con2 = new Contact();

        con1.setName("John Doe");
        con1.setNumbers("+639152468596");
        con1.setNumbers("+639228547963");
        con1.setAddresses("my office in Makati City");
        con1.setAddresses("my home in Quezon City");

        con2.setName("Jane Doe");
        con2.setNumbers("+639162148573");
        con2.setNumbers("+639173698541");
        con2.setAddresses("my home in Caloocan City");
        con2.setAddresses("my office in Pasay City");

        //13.
        phonebook.setContacts(con1);
        phonebook.setContacts(con2);

        phonebook.print();


    }

}
