package com.zuitt.b193;

import java.sql.SQLOutput;
import java.util.ArrayList;

public class Phonebook {

    //8.
   private ArrayList<Contact> contacts = new ArrayList<>();

    //9.
    public Phonebook(){};
    public Phonebook(ArrayList<Contact> contacts){
        this.contacts= contacts;
    }

    //10.
    public ArrayList<Contact> getContacts() {

        return this.contacts;
    }

    public void setContacts(Contact contacts) {

        this.contacts.add(contacts);
    }

    public void print(){
        if(contacts.isEmpty()){
            System.out.println("Phonebook is empty. ");
        }else{
            for (Contact con: getContacts()){
                con.contactDetails();
            }

        }
    }

}
